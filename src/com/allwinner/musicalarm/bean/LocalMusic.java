package com.allwinner.musicalarm.bean;

public class LocalMusic {

	public String	mTitle;
    public String   mPath;

    public LocalMusic(String title, String path) {
        this.mTitle = title;
        this.mPath = path;
    }
    
    public void setTitle(String title){
    	this.mTitle = title;
    }
    
    public void setPath(String path){
    	this.mPath = path;
    }
    
    public String getTitle(){
    	return this.mTitle;
    }
    
    public String getPath(){
    	return this.mPath;
    }
}
