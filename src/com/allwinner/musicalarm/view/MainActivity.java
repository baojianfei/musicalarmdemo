package com.allwinner.musicalarm.view;

import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.allwinner.musicalarm.R;
import com.allwinner.musicalarm.presenter.AlarmPresenter;
import com.allwinner.musicalarm.utils.Constants;

public class MainActivity extends Activity implements OnClickListener,
		IMainView {

	private Button mEnableBtn;
	private TextView mTextMusicName;
	private TextView mTextStartTime;
	private TextView mTextCurTime;

	private Button mBtnSelectMusic;
	private Button mBtnSetTime;

	private AlarmPresenter mPresenter;
	private String mMusicName = null;
	private String mMusicPath = null;
	private Calendar mCalendar = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (!isTaskRoot()) { 
			finish(); 
			return; 
		}
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		mPresenter = new AlarmPresenter(this);
		initView();
		
	}

	private void initView() {
		mEnableBtn = (Button) findViewById(R.id.btn_start_play_alarm);
		mTextMusicName = (TextView) findViewById(R.id.music_name_id);
		mTextStartTime = (TextView) findViewById(R.id.music_start_time);
		mBtnSelectMusic = (Button) findViewById(R.id.btn_select_music_id);
		mBtnSetTime = (Button) findViewById(R.id.btn_set_time);
		mTextCurTime = (TextView) findViewById(R.id.txt_cur_time);

		mEnableBtn.setOnClickListener(this);
		mBtnSelectMusic.setOnClickListener(this);
		mBtnSetTime.setOnClickListener(this);
		mPresenter.setCurTimeText();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_select_music_id:
			mPresenter.showSelectMusicView();
			break;
		case R.id.btn_set_time:
			mPresenter.showSetTimeDialog();
			break;
		case R.id.btn_start_play_alarm:
			if(mMusicName == null || mCalendar ==null){
				Toast.makeText(getApplicationContext(), "先设置音乐和时间", Toast.LENGTH_SHORT).show();
			}else{
				Log.d("bjf","===="+"musicName= "+mMusicName+" musicPath = "+mMusicPath);
				mPresenter.startPlayAlarm(this, mMusicName, mMusicPath, mCalendar);
			}
			break;
		}
	}

	@Override
	public void SetMusicName(String name) {
		mTextMusicName.setText(name);
	}

	@Override
	public void SetPlayTime(String time) {
		mTextStartTime.setText(time);
	}

	@Override
	public Context getInstance() {
		return this;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == Constants.START_ACTIVITY_REQUEST_CODE
				&& resultCode == RESULT_OK) {
			mMusicName = data.getStringExtra(Constants.RESULT_NAME);
			mMusicPath = data.getStringExtra(Constants.RESULT_PATH);
			mPresenter.setMainViewMusicText(mMusicName);
		}
	}

	@Override
	public void onSetTimeFinished(Calendar c) {
		mCalendar = c;
		mPresenter.setMainViewTimeText(c);
	}

	@Override
	public void setCurTime(String hour, String min, String second, String milliSecond) {
		mTextCurTime.setText(hour+" : "+ min + " : "+ second+" : "+milliSecond);
	}

}
