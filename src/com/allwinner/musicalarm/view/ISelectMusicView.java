package com.allwinner.musicalarm.view;

import java.util.ArrayList;

import android.content.Context;

import com.allwinner.musicalarm.bean.LocalMusic;

public interface ISelectMusicView {
	
	public ArrayList<LocalMusic> getMusicArray();
	
	public Context getInstance();
	
}
