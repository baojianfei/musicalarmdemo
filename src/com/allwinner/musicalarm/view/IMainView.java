package com.allwinner.musicalarm.view;

import java.util.Calendar;

import android.content.Context;

public interface IMainView {
	public void SetMusicName(String name);
	
	public void SetPlayTime(String time);
	
	public void onSetTimeFinished(Calendar c);
	
	public Context getInstance();
	
	public void setCurTime(String hour, String min, String second, String milliSecond);
}
