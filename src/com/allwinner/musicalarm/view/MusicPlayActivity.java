package com.allwinner.musicalarm.view;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.allwinner.musicalarm.R;

public class MusicPlayActivity extends Activity implements OnClickListener{
	
	private Button mBtnPause = null;
	private Button mBtnPlayBack = null;
	private MediaPlayer mPlayer = null;
//	private String mMusicName = null;          //音乐名，如果加TextView可以赋值
	private String mMusicPath = null;
	private Intent mIntent = null;
	private Uri mMusicUri = null;
	private boolean mPlaying = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.music_play);
		init();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		if(mPlayer == null){
			mPlayer = MediaPlayer.create(this, mMusicUri);
		}
		playMusic();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
//		releaseMusic();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	private void init(){
		mBtnPause = (Button) findViewById(R.id.btn_pause);
		mBtnPlayBack = (Button) findViewById(R.id.btn_play_back);
		
		mBtnPause.setOnClickListener(this);
		mBtnPlayBack.setOnClickListener(this);
		
		mIntent = this.getIntent();
//		mMusicName = mIntent.getStringExtra("music_name");     //从intent中获取音乐名
		mMusicPath = mIntent.getStringExtra("music_path");
		mMusicUri = Uri.parse(mMusicPath);
	}
	
	private void playMusic(){
		if (!mPlayer.isPlaying()) {
			mPlayer.setLooping(true);
			mPlayer.start();
			mPlaying = true;
		}
	}
	
	private void stopMusic(){
		if(mPlaying){
			mPlayer.pause();
			mPlaying = false;
			mBtnPause.setText(R.string.btn_start_play);
		}else{
			mPlaying = true;
			mPlayer.start();
			mBtnPause.setText(R.string.btn_pause_string);
		}
	}
	
	private void releaseMusic(){
		if (mPlayer != null) {
			mPlayer.stop();
			mPlayer.release();
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btn_pause:
			stopMusic();
			break;
		case R.id.btn_play_back:
			moveTaskToBack(true);
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			releaseMusic();
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}
	
	
}
