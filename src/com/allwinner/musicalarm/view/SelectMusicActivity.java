package com.allwinner.musicalarm.view;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.allwinner.musicalarm.R;
import com.allwinner.musicalarm.bean.LocalMusic;
import com.allwinner.musicalarm.presenter.SelectMusicPresenter;
import com.allwinner.musicalarm.utils.Constants;
import com.allwinner.musicalarm.utils.MusicAdapter;

public class SelectMusicActivity extends Activity implements OnClickListener,
		ISelectMusicView, OnItemClickListener {

	private ListView mMusicList;
	private Button mBtnOk;
	private Button mBtnCancel;
	private SelectMusicPresenter mPresenter;
	private ArrayList<LocalMusic> mLocalMusicArray = new ArrayList<LocalMusic>();
	private MusicAdapter mAdapter = null;
	private String mMusicName = null;
	private String mMusicPath = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.select_music);
		initView();
		initData();
	}

	private void initView() {
		mMusicList = (ListView) findViewById(R.id.music_list_id);
		mBtnOk = (Button) findViewById(R.id.btn_select_music_ok);
		mBtnCancel = (Button) findViewById(R.id.btn_select_music_cancel);

		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);

	}

	private void initData() {
		mPresenter = new SelectMusicPresenter(this);
		mLocalMusicArray = mPresenter.getMusicData();
		mAdapter = new MusicAdapter(mLocalMusicArray, this);

		mMusicList.setAdapter(mAdapter);
		mMusicList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mMusicList.setOnItemClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_select_music_ok:
			Intent intent = new Intent();
			intent.putExtra(Constants.RESULT_NAME, mMusicName);
			intent.putExtra(Constants.RESULT_PATH, mMusicPath);
			setResult(RESULT_OK, intent);
			finish();
			break;
		case R.id.btn_select_music_cancel:
			finish();
			break;
		}
	}

	@Override
	public ArrayList<LocalMusic> getMusicArray() {
		return mLocalMusicArray;
	}

	@Override
	public Context getInstance() {
		return this;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		mMusicName = mLocalMusicArray.get(position).mTitle;
		mMusicPath = mLocalMusicArray.get(position).mPath;
		mAdapter.setSelcetItem(position);
		mAdapter.notifyDataSetChanged();
	}

}
