package com.allwinner.musicalarm.model;

import java.util.Calendar;

import android.content.Context;

import com.allwinner.musicalarm.view.IMainView;

public interface IAlarmModel {
	
	public Calendar ShowSetTimeDialog(Context context);
	
	public void ShowSelectMusicView(Context context);
	
	public void setIMainViewInterface(IMainView view);
	
	public void startAlarm(Context context, String name, String path, Calendar c);
	
}
