package com.allwinner.musicalarm.model;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.TimePicker;

import com.allwinner.musicalarm.receiver.AlarmReceiver;
import com.allwinner.musicalarm.utils.Constants;
import com.allwinner.musicalarm.view.IMainView;
import com.allwinner.musicalarm.view.SelectMusicActivity;

public class AlarmModel implements IAlarmModel {

	final Calendar c = Calendar.getInstance();

	private TimePickerDialog mTimePickerDialog = null;

	private IMainView mIMainView = null;
	
	public static int currentIntent = 0;

	public AlarmModel() {

	}

	@Override
	public Calendar ShowSetTimeDialog(Context context) {
		return craeteTimePickerDialog(context);
	}

	@Override
	public void ShowSelectMusicView(Context context) {
		Intent intent = new Intent();
		intent.setClass(context, SelectMusicActivity.class);
		((Activity) context).startActivityForResult(intent,
				Constants.START_ACTIVITY_REQUEST_CODE);
	}

	public Calendar craeteTimePickerDialog(Context context) {
		c.setTimeInMillis(System.currentTimeMillis());
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int minute = c.get(Calendar.MINUTE);
		mTimePickerDialog = new TimePickerDialog(context,
				new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker view, int hourOfDay,
							int minute) {
						// TODO Auto-generated method stub
						c.setTimeInMillis(System.currentTimeMillis());
						c.set(Calendar.HOUR_OF_DAY, hourOfDay);
						c.set(Calendar.MINUTE, minute);
						c.set(Calendar.SECOND, 0); // 设为 0
						c.set(Calendar.MILLISECOND, 0); // 设为 0

						if (mIMainView != null) {
							mIMainView.onSetTimeFinished(c);
						}

					}
				}, hour, minute, true);
		mTimePickerDialog.show();
		return c;
	}

	@Override
	public void setIMainViewInterface(IMainView view) {
		mIMainView = view;
	}

	@Override
	public void startAlarm(Context context, String name, String path, Calendar c) {
		Intent intent = new Intent();
		intent.setClass(context, AlarmReceiver.class);
		intent.putExtra("music_name", name);
		intent.putExtra("music_path", path);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, currentIntent++);
		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		am.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pi);
	}
}
