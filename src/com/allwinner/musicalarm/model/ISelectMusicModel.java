package com.allwinner.musicalarm.model;

import java.util.ArrayList;

import android.content.Context;

import com.allwinner.musicalarm.bean.LocalMusic;

public interface ISelectMusicModel {
	
	public ArrayList<LocalMusic> getMusicData(Context context);
	
}
