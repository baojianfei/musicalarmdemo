package com.allwinner.musicalarm.model;

import java.util.ArrayList;

import android.content.Context;

import com.allwinner.musicalarm.bean.LocalMusic;
import com.allwinner.musicalarm.utils.MediaUtils;

public class SelectMusicModel implements ISelectMusicModel{

	@Override
	public ArrayList<LocalMusic> getMusicData(Context context) {
		return (ArrayList<LocalMusic>) MediaUtils.initLocalSongList(context);
	}

}
