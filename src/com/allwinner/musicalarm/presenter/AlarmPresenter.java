package com.allwinner.musicalarm.presenter;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

import com.allwinner.musicalarm.model.AlarmModel;
import com.allwinner.musicalarm.model.IAlarmModel;
import com.allwinner.musicalarm.view.IMainView;

public class AlarmPresenter {
	
	private IMainView mMainView;
	private IAlarmModel mAlarmModel;
	private Context mContext;
	
	public AlarmPresenter(IMainView view){
		this.mMainView = view;
		this.mAlarmModel = new AlarmModel();
		mContext = mMainView.getInstance();
	}
	
	public void showSetTimeDialog(){
		mAlarmModel.ShowSetTimeDialog(mContext);
		mAlarmModel.setIMainViewInterface(mMainView);
	}
	
	public void showSelectMusicView(){
		mAlarmModel.ShowSelectMusicView(mContext);
	}
	
	public void setMainViewTimeText(Calendar c){
		if(c == null){
			return;
		}
		String time = c.get(Calendar.HOUR_OF_DAY) + " : " + c.get(Calendar.MINUTE);
		mMainView.SetPlayTime(time);
	}
	
	public void setMainViewMusicText(String title){
		mMainView.SetMusicName(title);
	}
	
	public void onSetTimeFinished(Calendar c){
		mMainView.onSetTimeFinished(c);
	}
	
	public void startPlayAlarm(Context context, String name, String path, Calendar c){
		mAlarmModel.startAlarm(context, name, path, c);
	}
	
	public void setCurTimeText(){
		Thread thread = new Thread() {
			public void run() {
				while (true) {
					Calendar c = Calendar.getInstance();
					Message msg = new Message();
					Bundle bundle = new Bundle();
					bundle.putInt("hour", c.get(Calendar.HOUR_OF_DAY));
					bundle.putInt("min", c.get(Calendar.MINUTE));
					bundle.putInt("second", c.get(Calendar.SECOND));
					bundle.putInt("milliSecond", c.get(Calendar.MILLISECOND));
					msg.setData(bundle);
					mHandler.sendMessage(msg);
					SystemClock.sleep(1);
				}
			}
		};
		thread.start();
	}
	
	@SuppressLint("HandlerLeak") 
	private Handler mHandler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			Bundle bundle = msg.getData();
			int hour = bundle.getInt("hour");
			int min = bundle.getInt("min");
			int second = bundle.getInt("second");
			int milliSecond = bundle.getInt("milliSecond");
			String strMilli = null;
			String strHour = null;
			String strMin = null;
			String strSecond = null;
			if(milliSecond < 10){
				strMilli = "00"+milliSecond;
			}else if(milliSecond < 100){
				strMilli = "0" + milliSecond;
			}else {
				strMilli = milliSecond+"";
			}
			if(second < 10){
				strSecond = "0"+second;
			}else{
				strSecond = second +"";
			}
			if(min < 10){
				strMin = "0"+ min;
			}else{
				strMin = min +"";
			}
			strHour = hour+"";
			mMainView.setCurTime(strHour, strMin, strSecond, strMilli);
		}
		
	};

}
