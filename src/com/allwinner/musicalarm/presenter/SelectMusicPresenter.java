package com.allwinner.musicalarm.presenter;

import java.util.ArrayList;

import android.content.Context;

import com.allwinner.musicalarm.bean.LocalMusic;
import com.allwinner.musicalarm.model.ISelectMusicModel;
import com.allwinner.musicalarm.model.SelectMusicModel;
import com.allwinner.musicalarm.view.ISelectMusicView;

public class SelectMusicPresenter {
	
	private ISelectMusicView mView;
	private ISelectMusicModel mModel;
	private Context mContext;
	
	public SelectMusicPresenter(ISelectMusicView view){
		this.mView = view;
		this.mModel = new SelectMusicModel();
		mContext = mView.getInstance();
	}
	
	public ArrayList<LocalMusic> getMusicData(){
		return mModel.getMusicData(mContext);
	}
}
