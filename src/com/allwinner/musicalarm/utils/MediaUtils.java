package com.allwinner.musicalarm.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.allwinner.musicalarm.bean.LocalMusic;

import java.util.ArrayList;
import java.util.List;

public class MediaUtils {
	// 加载手机里面的本地音乐-->sqlite-->contentProvider
	/**
	 * 加载本地的音乐
	 * 
	 * @param context
	 */
	public static List<LocalMusic> initLocalSongList(Context context) {
		List<LocalMusic> songList = new ArrayList<LocalMusic>();
		Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;// uri
		/**
		 * public String name;//歌曲名 public String author;//歌手 public String
		 * path;//路径 public long duration;//播放时长
		 */
		String[] projection = { MediaStore.Audio.Media.TITLE,
				MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.DATA,
				MediaStore.Audio.Media.MIME_TYPE };
		Cursor c = context.getContentResolver().query(uri, projection,
				MediaStore.Audio.Media.MIME_TYPE + "!= 'audio/aac-adts'", null,
				null);
		while (c.moveToNext()) {
			String title = c.getString(c
					.getColumnIndex(MediaStore.Audio.Media.TITLE));
			String path = c.getString(c
					.getColumnIndex(MediaStore.Audio.Media.DATA));
			LocalMusic music = new LocalMusic(title, path);
			songList.add(music);
		}
		c.close();
		return songList;
	}

	/**
	 * 查询本地的音乐
	 * 
	 * @param context
	 */
	public static List<LocalMusic> queryLocalSongList(Context context,
			String query) {
		List<LocalMusic> songList = new ArrayList<LocalMusic>();
		Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;// uri
		/**
		 * public String name;//歌曲名 public String author;//歌手 public String
		 * path;//路径 public long duration;//播放时长
		 */
		String[] projection = { MediaStore.Audio.Media.TITLE,
				MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.DATA,
				MediaStore.Audio.Media.MIME_TYPE };
		Cursor c = context.getContentResolver().query(
				uri,
				projection,
				MediaStore.Audio.Media.MIME_TYPE + "!= 'audio/aac-adts'"
						+ "  AND " + MediaStore.Audio.Media.TITLE + " like '%"
						+ query + "%'", null, null);
		while (c.moveToNext()) {
			String title = c.getString(c
					.getColumnIndex(MediaStore.Audio.Media.TITLE));
			String path = c.getString(c
					.getColumnIndex(MediaStore.Audio.Media.DATA));
			LocalMusic music = new LocalMusic(title, path);
			songList.add(music);
		}
		c.close();
		return songList;
	}

	public static String duration2Str(int duration) {
		// "00:11" "11:11"
		String result = "";
		int i = duration / 1000;
		int min = i / 60;// 1 2 3
		int sec = i % 60;// 0-59
		if (min > 9) {
			if (sec > 9) {
				result = min + ":" + sec;
			} else {
				result = min + ":0" + sec;
			}
		} else {
			if (sec > 9) {
				result = "0" + min + ":" + sec;
			} else {
				result = "0" + min + ":0" + sec;
			}
		}
		return result;
	}

	/**
	 * 转换和添加播放时长
	 * 
	 * @param duration
	 */
	public static String addLength(int duration) {
		int minute = duration / 60;
		int second = duration - minute * 60;
		String minu = null;
		String sec = null;
		if (minute < 10) {
			minu = "0" + minute;
		} else {
			minu = minute + "";
		}
		if (second == 0) {
			sec = "00";
		} else if (second < 10) {
			sec = "0" + second;
		} else {
			sec = second + "";
		}
		if (minute == 0) {
			return "时长 : " + sec + "秒";
		} else {
			return "时长 : " + minu + "分" + sec + "秒";
		}
	}

}
