package com.allwinner.musicalarm.utils;

public class Constants {
	public static int START_ACTIVITY_REQUEST_CODE = 1;
	
	public static int RESULT_OK = -1;
	
	public static String RESULT_NAME = "name";
	
	public static String RESULT_PATH = "path";
	
}
