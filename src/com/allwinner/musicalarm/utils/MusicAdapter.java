package com.allwinner.musicalarm.utils;

import java.util.ArrayList;

import com.allwinner.musicalarm.bean.LocalMusic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.allwinner.musicalarm.R;

public class MusicAdapter extends BaseAdapter{
	
	private ArrayList<LocalMusic> mMusicArrayList = new ArrayList<LocalMusic>();
	private LayoutInflater mInflater;
	private int selectIndex = -1;
	
	public MusicAdapter(ArrayList<LocalMusic> list, Context context){
		mMusicArrayList = list;
		mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return mMusicArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public void setSelcetItem(int position){
		this.selectIndex = position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if(convertView == null){
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.listview_item, null);
			holder.musicName = (TextView) convertView.findViewById(R.id.music_name_id);
			holder.musicSelect = (ImageView) convertView.findViewById(R.id.select_imagebtn_btn);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.musicName.setText(mMusicArrayList.get(position).mTitle);
		
		if(position == selectIndex){
			holder.musicSelect.setBackgroundResource(R.drawable.checked);
		}else{
			holder.musicSelect.setBackgroundResource(R.drawable.pressed);
		}
		return convertView;
	}
	
	static class ViewHolder{
		public TextView musicName;
		public ImageView musicSelect;
	}

}
