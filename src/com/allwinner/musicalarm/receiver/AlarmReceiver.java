package com.allwinner.musicalarm.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.allwinner.musicalarm.view.MusicPlayActivity;

public class AlarmReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		
		String name = intent.getStringExtra("music_name");
		String path = intent.getStringExtra("music_path");
		Intent i = new Intent(context, MusicPlayActivity.class);
		i.putExtra("music_name", name);
		i.putExtra("music_path", path);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}
}
